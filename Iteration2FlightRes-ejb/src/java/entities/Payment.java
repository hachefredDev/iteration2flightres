/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author NOYZ3
 */
public class Payment {
    private int id;
    private String ccType;
    private String ccNumber;
    private String ccHolderName;
    private String ccExpiry;
    private String securityCode;
    private String addrLine1;
    private String addrLine2;

    public Payment() {
    }

    public Payment(int id, String ccType, String ccNumber, String ccHolderName, String ccExpiry, String securityCode, String addrLine1, String addrLine2) {
        this.id = id;
        this.ccType = ccType;
        this.ccNumber = ccNumber;
        this.ccHolderName = ccHolderName;
        this.ccExpiry = ccExpiry;
        this.securityCode = securityCode;
        this.addrLine1 = addrLine1;
        this.addrLine2 = addrLine2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCcType() {
        return ccType;
    }

    public void setCcType(String ccType) {
        this.ccType = ccType;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(String ccNumber) {
        this.ccNumber = ccNumber;
    }

    public String getCcHolderName() {
        return ccHolderName;
    }

    public void setCcHolderName(String ccHolderName) {
        this.ccHolderName = ccHolderName;
    }

    public String getCcExpiry() {
        return ccExpiry;
    }

    public void setCcExpiry(String ccExpiry) {
        this.ccExpiry = ccExpiry;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public String getAddrLine1() {
        return addrLine1;
    }

    public void setAddrLine1(String addrLine1) {
        this.addrLine1 = addrLine1;
    }

    public String getAddrLine2() {
        return addrLine2;
    }

    public void setAddrLine2(String addrLine2) {
        this.addrLine2 = addrLine2;
    }
    
    
}
