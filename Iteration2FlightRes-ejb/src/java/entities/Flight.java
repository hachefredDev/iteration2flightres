/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author frede
 */
public class Flight {
     private String flightNumber;
    private String airlineName;
    private int alliance;
    private String depIATA;
    private String arvlIATA;
    private Date depDate;
    private Date arvlDate;
    private Date totalFlightTime;
    private Status status;
    private int totalSeatAmnt;
    private int totalAvailableSeat;
    private int fcSeatAmnt;
    private int bcSeatAmnt;
    private int ecSeatAmnt;
    private String cancelMsg;
    private Timestamp cancelDT;
    
    
    public Flight() {
    }

    public Flight(String flightNumber, String airlineName, String depIATA, String arvlIATA, Date depDate, Date arvlDate, Date totalFlightTime, int status, int totalSeatAmnt, int totalAvailableSeat) throws Exception {
        this.flightNumber = flightNumber;
        this.airlineName = airlineName;
        this.depIATA = depIATA;
        this.arvlIATA = arvlIATA;
        this.depDate = depDate;
        this.arvlDate = arvlDate;
        this.totalFlightTime = totalFlightTime;
        this.status = getStatusFromValue(status);
        this.totalSeatAmnt = totalSeatAmnt;
        this.totalAvailableSeat = totalAvailableSeat;

    }

    public String getCancelMsg() {
        return cancelMsg;
    }

    public void setCancelMsg(String cancelMsg) {
        this.cancelMsg = cancelMsg;
    }

    public Timestamp getCancelDT() {
        return cancelDT;
    }

    public void setCancelDT(Timestamp cancelDT) {
        this.cancelDT = cancelDT;
    }

    
    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public int getAlliance() {
        return alliance;
    }

    public void setAlliance(int alliance) {
        this.alliance = alliance;
    }

    public String getDepIATA() {
        return depIATA;
    }

    public void setDepIATA(String depIATA) {
        this.depIATA = depIATA;
    }

    public String getArvlIATA() {
        return arvlIATA;
    }

    public void setArvlIATA(String arvlIATA) {
        this.arvlIATA = arvlIATA;
    }

    public Date getDepDate() {
        return depDate;
    }

    public void setDepDate(Date depDate) {
        this.depDate = depDate;
    }

    public Date getArvlDate() {
        return arvlDate;
    }

    public void setArvlDate(Date arvlDate) {
        this.arvlDate = arvlDate;
    }

    public Date getTotalFlightTime() {
        return totalFlightTime;
    }

    public void setTotalFlightTime(Date totalFlightTime) {
        this.totalFlightTime = totalFlightTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getTotalSeatAmnt() {
        return totalSeatAmnt;
    }

    public void setTotalSeatAmnt(int totalSeatAmnt) {
        this.totalSeatAmnt = totalSeatAmnt;
    }

    public int getTotalAvailableSeat() {
        return totalAvailableSeat;
    }

    public void setTotalAvailableSeat(int totalAvailableSeat) {
        this.totalAvailableSeat = totalAvailableSeat;
    }

    public int getFcSeatAmnt() {
        return fcSeatAmnt;
    }

    public void setFcSeatAmnt(int fcSeatAmnt) {
        this.fcSeatAmnt = fcSeatAmnt;
    }

    public int getBcSeatAmnt() {
        return bcSeatAmnt;
    }

    public void setBcSeatAmnt(int bcSeatAmnt) {
        this.bcSeatAmnt = bcSeatAmnt;
    }

    public int getEcSeatAmnt() {
        return ecSeatAmnt;
    }

    public void setEcSeatAmnt(int ecSeatAmnt) {
        this.ecSeatAmnt = ecSeatAmnt;
    }

    public java.sql.Date getDepTimeFormated(){
        return new java.sql.Date(depDate.getTime());
    }
    
    public java.sql.Date getArvlTimeFormated(){
        return new java.sql.Date(arvlDate.getTime());
    }
    
    public Timestamp getTotalFlightTimeFormated(){
        Timestamp a = new Timestamp(this.depDate.getTime());
        Timestamp b = new Timestamp(this.arvlDate.getTime());
        

        long diff = a.getTime() - b.getTime();
        Timestamp totalTime = new Timestamp(diff);
        
        return totalTime;
    }
    
    public int getStatusValue(){
       int value = 0;
       Status s = this.status;
       switch(s){
           case OPEN: value = 0; break;
           case IN_TRANSIT: value = 1; break;
           case DELAYED: value = 2; break;
           case COMPLETED: value = 3; break;
           case CANCELLED: value = 4; break;
       }
       return value;
    }
    
    public Status getStatusFromValue(int i) throws Exception{
        Status s;
        switch(i){
            case 0: s = Status.OPEN; break;
            case 1: s = Status.IN_TRANSIT; break;
            case 2: s = Status.DELAYED; break;
            case 3: s = Status.COMPLETED; break;
            case 4: s = Status.CANCELLED; break;
            default:
                throw IllegalArgumentException("Invalid value");
        }
        
        return s;
    }
    /*
    public java.sql.Date getDtformattedbmi() {       
        return new java.sql.Date(dtbmi.getTime());
    }*/

    private Exception IllegalArgumentException(String invalid_value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
