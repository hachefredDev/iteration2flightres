/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author frede
 */
public class Airport {
    private String iata;
    private String airportname;
    private String city;
    private String province;
    private String localtimezone;
    
    public Airport(){
        
    }

    public Airport(String iata, String airportname, String city, String province, String localtimezone) {
        this.iata = iata;
        this.airportname = airportname;
        this.city = city;
        this.province = province;
        this.localtimezone = localtimezone;
    }

    public Airport(String iata, String city) {
        this.iata = iata;
        this.city = city;
    }
    
    

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getAirportname() {
        return airportname;
    }

    public void setAirportname(String airportname) {
        this.airportname = airportname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getLocaltimezone() {
        return localtimezone;
    }

    public void setLocaltimezone(String localtimezone) {
        this.localtimezone = localtimezone;
    }
    
    
}
