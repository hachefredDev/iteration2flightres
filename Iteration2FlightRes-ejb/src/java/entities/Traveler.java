/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author NOYZ3
 */
public class Traveler {
    private int id;
    private String firstName;
    private String lastName;
    private Boolean gender;
    private String phoneNumber;
    private String PassportCountry;
    private Date dob;
    private String frequentFlyerProgram;
    private String specialNeedReq;
    private String otherNeeds;
    private String email;

    public Traveler() {
    }

    public Traveler(int id, String firstName, String lastName, Boolean gender, String phoneNumber, String PassportCountry, Date dob, String frequentFlyerProgram, String specialNeedReq, String otherNeeds, String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.phoneNumber = phoneNumber;
        this.PassportCountry = PassportCountry;
        this.dob = dob;
        this.frequentFlyerProgram = frequentFlyerProgram;
        this.specialNeedReq = specialNeedReq;
        this.otherNeeds = otherNeeds;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassportCountry() {
        return PassportCountry;
    }

    public void setPassportCountry(String PassportCountry) {
        this.PassportCountry = PassportCountry;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getFrequentFlyerProgram() {
        return frequentFlyerProgram;
    }

    public void setFrequentFlyerProgram(String frequentFlyerProgram) {
        this.frequentFlyerProgram = frequentFlyerProgram;
    }

    public String getSpecialNeedReq() {
        return specialNeedReq;
    }

    public void setSpecialNeedReq(String specialNeedReq) {
        this.specialNeedReq = specialNeedReq;
    }

    public String getOtherNeeds() {
        return otherNeeds;
    }

    public void setOtherNeeds(String otherNeeds) {
        this.otherNeeds = otherNeeds;
    }
    
    
}
