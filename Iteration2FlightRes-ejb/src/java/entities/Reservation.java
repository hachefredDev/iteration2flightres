/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.util.Date;

/**
 *
 * @author NOYZ3
 */
public class Reservation {
    private int id;
    private Date deptTime;
    private Date arrTime;
    private int flyingHours;
    private String airlineName;
    private String flightNumber;
    private String aircraftName;
    private String status;

    public Reservation() {
    }

    public Reservation(int id, String resNumber, Date deptTime, Date arrTime, int flyingHours, String airlineName, String flightNumber, String aircraftName, String status) {
        this.id = id;
        this.deptTime = deptTime;
        this.arrTime = arrTime;
        this.flyingHours = flyingHours;
        this.airlineName = airlineName;
        this.flightNumber = flightNumber;
        this.aircraftName = aircraftName;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDeptTime() {
        return deptTime;
    }

    public void setDeptTime(Date deptTime) {
        this.deptTime = deptTime;
    }

    public Date getArrTime() {
        return arrTime;
    }

    public void setArrTime(Date arrTime) {
        this.arrTime = arrTime;
    }

    public int getFlyingHours() {
        return flyingHours;
    }

    public void setFlyingHours(int flyingHours) {
        this.flyingHours = flyingHours;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAircraftName() {
        return aircraftName;
    }

    public void setAircraftName(String aircraftName) {
        this.aircraftName = aircraftName;
    }
    
    
}
