/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author NOYZ3
 */
public class Ticket {
    private int id;
    private Double price;
    private Double airportTransportCharges;
    private Double taxes;
    private Double fees;

    public Ticket() {
    }

    public Ticket(int id, Double price, Double airportTransportCharges, Double taxes, Double fees) {
        this.id = id;
        this.price = price;
        this.airportTransportCharges = airportTransportCharges;
        this.taxes = taxes;
        this.fees = fees;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getAirportTransportCharges() {
        return airportTransportCharges;
    }

    public void setAirportTransportCharges(Double airportTransportCharges) {
        this.airportTransportCharges = airportTransportCharges;
    }

    public Double getTaxes() {
        return taxes;
    }

    public void setTaxes(Double taxes) {
        this.taxes = taxes;
    }

    public Double getFees() {
        return fees;
    }

    public void setFees(Double fees) {
        this.fees = fees;
    }
    
    
}
