/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import com.sun.rowset.CachedRowSetImpl;
import entities.Airport;
import entities.Flight;
import entities.Status;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;

/**
 *
 * @author frede
 */
@Stateless
@LocalBean
public class FlightSB {

    @Resource(name = "jdbc/flightreservation")
    DataSource dataSource;
  
  private ArrayList<Flight> flights = new ArrayList<>();
  private ArrayList<Airport> airports = new ArrayList<>();
  
  public String cancelFlight() throws SQLException{

      for (Flight f : flights){
        if (dataSource == null){
            throw new SQLException("Unable to obtain Datasource");
      }
      Connection connection = dataSource.getConnection();
        if (connection == null){
          throw new SQLException("Unable to connect to the DataSource");
      }
        if (f.getStatus() == Status.IN_TRANSIT || f.getStatus() == Status.COMPLETED){
            return "The flight cannot be cancelled";
        }
        
        int cancelledStatus = 4;
        String cancellationMsg = "FLIGHT CANCELLED";
        Date cancelDate = new Date();
        
        try{
          String sql = "UPDATE flight SET flightStatus = ? , cancellationMsg = ?, cancelDateTime = ? WHERE flightNumber = ? AND flightStatus = 0";
          PreparedStatement modF = connection.prepareStatement(sql);
          modF.setInt(1, cancelledStatus);
          modF.setString(2, cancellationMsg);
          modF.setDate(3, (java.sql.Date) cancelDate);
          modF.setString(4, f.getFlightNumber());
          
          int done = modF.executeUpdate();
 
      }finally {
          connection.close();
      }
        
      }
      return "";
  }
   
  public ArrayList<Flight> getFlightById(String flightNumber) throws SQLException, Exception{
      
      System.out.println("Start of Retrieve");  
      if (dataSource == null) {
            throw new SQLException("Unable to obtain DataSource");
        }
        Connection connection = dataSource.getConnection();
        if (dataSource == null) {
            throw new SQLException("Unable to connect to DataSource");
        }
        
        System.out.println("Passed Data Connection");
        try{
            String sql = "SELECT flightNumber, airlineName, departureAirport, arrivalAirport, departureTime, arrivalTime, totalFlightTime, flightStatus, totalSeats, totalAvailableSeats from flight WHERE flightNumber = ?";
            PreparedStatement getF = connection.prepareStatement(sql);
            getF.setString(1, flightNumber);
            CachedRowSet rowSet = new CachedRowSetImpl();
            rowSet.populate(getF.executeQuery());
            
            while(rowSet.next()){
                String ffnum = rowSet.getString(1);
                String fairname = rowSet.getString(2);
                String fdairport = rowSet.getString(3);
                String faairport = rowSet.getString(4);
                Timestamp fdepdatetime = new Timestamp(rowSet.getTime(5).getTime());
                System.out.println(fdepdatetime);
                Timestamp farrdatetime = new Timestamp(rowSet.getTime(6).getTime());
                Timestamp ftotalft = new Timestamp(rowSet.getTime(7).getTime());
                int fstatusint = rowSet.getInt(8);
                int ftotalseat = rowSet.getInt(9);
                int ftotalavseat = rowSet.getInt(10);
                
                Flight f = new Flight(ffnum, fairname, fdairport, faairport, fdepdatetime, farrdatetime, ftotalft, fstatusint, ftotalseat, ftotalavseat);
                System.out.println(f);
                System.out.println(f.getAirlineName());
                flights.add(f);
            }
            System.out.println("Filled Flights");
        }finally{
            connection.close();
        }
       
        System.out.println("Rendered is cleared");
        return flights;
  }

  public ArrayList<Flight> getFlights() {
        return flights;
    }
  
  public ArrayList<Airport> getAirportList() throws SQLException{
      if (dataSource == null) {
            throw new SQLException("Unable to obtain DataSource");
        }
        Connection connection = dataSource.getConnection();
        if (dataSource == null) {
            throw new SQLException("Unable to connect to DataSource");
        }
        
        try{
            String sql = "SELECT IATACode, city FROM airport";
            PreparedStatement getAirport = connection.prepareStatement(sql);           
            CachedRowSet rowSet = new CachedRowSetImpl();
            rowSet.populate(getAirport.executeQuery());
            
            while(rowSet.next()){
                String iatacode = rowSet.getString(1);
                String city = rowSet.getString(2);
                
                Airport ap = new Airport(iatacode, city);
                airports.add(ap);
            }
            //System.out.println("Filled Flights");
        }finally{
            connection.close();
        }
       
        //System.out.println("Rendered is cleared");
        return airports;
  }
 
  public ArrayList<Flight> searchFlights(Airport departure, Airport arrival) throws SQLException, Exception{
      if (dataSource == null) {
            throw new SQLException("Unable to obtain DataSource");
        }
        Connection connection = dataSource.getConnection();
        if (dataSource == null) {
            throw new SQLException("Unable to connect to DataSource");
        }
        try{
            String sql = "SELECT flightNumber, airlineName, departureAirport, arrivalAirport, departureTime, arrivalTime, totalFlightTime, flightStatus, totalSeats, totalAvailableSeats from flight WHERE departureAirport = ? AND arrivalAirport = ?";
            PreparedStatement getF = connection.prepareStatement(sql);
            System.out.println(departure.getIata());
            System.out.println(arrival.getIata());
            getF.setString(1, departure.getIata());
            getF.setString(2, arrival.getIata());
            CachedRowSet rowSet = new CachedRowSetImpl();
            rowSet.populate(getF.executeQuery());
            
            while(rowSet.next()){
                String ffnum = rowSet.getString(1);
                String fairname = rowSet.getString(2);
                String fdairport = rowSet.getString(3);
                String faairport = rowSet.getString(4);
                Timestamp fdepdatetime = new Timestamp(rowSet.getTime(5).getTime());
                System.out.println(fdepdatetime);
                Timestamp farrdatetime = new Timestamp(rowSet.getTime(6).getTime());
                Timestamp ftotalft = new Timestamp(rowSet.getTime(7).getTime());
                int fstatusint = rowSet.getInt(8);
                int ftotalseat = rowSet.getInt(9);
                int ftotalavseat = rowSet.getInt(10);
                
                Flight f = new Flight(ffnum, fairname, fdairport, faairport, fdepdatetime, farrdatetime, ftotalft, fstatusint, ftotalseat, ftotalavseat);
                System.out.println(f);
                System.out.println(f.getAirlineName());
                flights.add(f);
            }
            System.out.println("Search Flight Filled Flights");
        }finally{
            connection.close();
        }
       
        System.out.println("Rendered is cleared");
        return flights;
  }
  
  public boolean addFlight(ArrayList<Flight> flights) throws SQLException{
      if (dataSource == null){
          throw new SQLException("Unable to obtain Datasource");
      }
      Connection connection = dataSource.getConnection();
      if (connection == null){
          throw new SQLException("Unable to connect to the DataSource");
      }
      
            //Business Rule 104:
      int openStatus = 0;
      
      for(Flight f : flights){
          
      
      try{
          String sql = "INSERT INTO flight ( flightNumber, airlineName, departureAirport, arrivalAirport, departureTime, arrivalTime, totalFlightTime, flightStatus, totalSeats, totalAvailableSeats, numFirstClassSeats, numBusinessSeats, numEconomySeats ) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
          PreparedStatement p = connection.prepareStatement(sql);
          p.setString(1, f.getFlightNumber());
          p.setString(2, f.getAirlineName());
          
          p.setString(3, f.getDepIATA());
          p.setString(4, f.getArvlIATA());
          p.setTime(5, (new java.sql.Time(f.getDepDate().getTime())));
          p.setTime(6, (new java.sql.Time(f.getArvlDate().getTime())));
          p.setTime(7, (new java.sql.Time(f.getTotalFlightTime().getTime())));
          p.setInt(8, openStatus);
          p.setInt(9, f.getTotalSeatAmnt());
          p.setInt(10, f.getTotalAvailableSeat());
          p.setInt(11, f.getFcSeatAmnt());
          p.setInt(12, f.getBcSeatAmnt());
          p.setInt(13, f.getEcSeatAmnt());
          
          int done = p.executeUpdate();
          
          
      } finally {
          connection.close();
      }
      }
      return true;
  }

  public boolean modifyFlight(Flight f) throws SQLException{
      if (dataSource == null){
          throw new SQLException("Unable to obtain Datasource");
      }
      Connection connection = dataSource.getConnection();
      if (connection == null){
          throw new SQLException("Unable to connect to the DataSource");
      }
      
      try{
          String sql = "UPDATE flight SET departureTime = ?, arrivalTime = ?, totalFlightTime = ?, flightStatus = ?, totalSeats = ?, totalAvailableSeats = ? WHERE flightNumber = ? ";
          PreparedStatement modF = connection.prepareStatement(sql);
          modF.setTime(1, (new java.sql.Time(f.getDepDate().getTime())));
          modF.setTime(2, (new java.sql.Time(f.getArvlDate().getTime())));
          modF.setTimestamp(3, f.getTotalFlightTimeFormated());
          modF.setInt(4, f.getStatusValue());
          modF.setInt(5, f.getTotalSeatAmnt());
          modF.setInt(6, f.getTotalAvailableSeat());
          modF.setString(7, f.getFlightNumber());
          
          int done = modF.executeUpdate();

          return true;
      }finally {
          connection.close();
      }
  }
  
  public String modifyFlightEx() throws SQLException{
      
      for (Flight f : flights){
          
        if (dataSource == null){
            throw new SQLException("Unable to obtain Datasource");
        }
        Connection connection = dataSource.getConnection();
        if (connection == null){
          throw new SQLException("Unable to connect to the DataSource");
        }
        
        if(f.getStatus() == Status.COMPLETED || f.getStatus() == Status.CANCELLED ){
            return "Cannot modify a flight that is completed or cancelled.";
        }
        
        if (f.getStatus() == Status.OPEN){
            //Business Rule 107: Can't modify flight number, airline name, dep. airport, arr. airport
            try{
              String sql = "UPDATE flight SET departureTime = ?, arrivalTime = ?, totalFlightTime = ?, flightStatus = ?, totalSeats = ?, totalAvailableSeats = ? WHERE flightNumber = ? ";
              PreparedStatement modF = connection.prepareStatement(sql);
              modF.setTime(1, (new java.sql.Time(f.getDepDate().getTime())));
              modF.setTime(2, (new java.sql.Time(f.getArvlDate().getTime())));
              modF.setTimestamp(3, f.getTotalFlightTimeFormated());
              modF.setInt(4, f.getStatusValue());
              modF.setInt(5, f.getTotalSeatAmnt());
              modF.setInt(6, f.getTotalAvailableSeat());
              modF.setString(7, f.getFlightNumber());

              int done = modF.executeUpdate();

              return "Flight Modified.";

          }finally {
              connection.close();
          }
        }
        
        if (f.getStatus() == Status.DELAYED || f.getStatus() == Status.IN_TRANSIT){
            //Alternate Course of event - Line 2
            try{
              String sql = "UPDATE flight SET departureTime = ?, arrivalTime = ?, totalFlightTime = ?, flightStatus = ? WHERE flightNumber = ? ";
              PreparedStatement modF = connection.prepareStatement(sql);
              modF.setTime(1, (new java.sql.Time(f.getDepDate().getTime())));
              modF.setTime(2, (new java.sql.Time(f.getArvlDate().getTime())));
              modF.setTimestamp(3, f.getTotalFlightTimeFormated());
              modF.setInt(4, f.getStatusValue());
              modF.setString(7, f.getFlightNumber());

              int done = modF.executeUpdate();

              return "Flight Modified.";

          }finally {
              connection.close();
          }
        }
      }
      return "";
  }
  
  public boolean deleteFlight(Flight f) throws SQLException{
      //Business Rule 108
      if (f.getStatus() != Status.OPEN){
          return false;
      }
      
      if (dataSource == null){
          throw new SQLException("Unable to obtain Datasource");
      }
      Connection connection = dataSource.getConnection();
    if (connection == null){
          throw new SQLException("Unable to connect to the DataSource");
      }
    try{
        String sql = "DELETE flight WHERE flightNumber = ? AND status = ?";
        PreparedStatement delF = connection.prepareStatement(sql);
        delF.setString(1, f.getFlightNumber());
        delF.setInt(2, 0);
        
        delF.executeUpdate();
        return true;
    } finally {
        connection.close();
    }
  }
  
  
}
