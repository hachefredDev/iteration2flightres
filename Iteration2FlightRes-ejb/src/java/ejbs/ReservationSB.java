/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import entities.Flight;
import entities.Payment;
import entities.Traveler;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author NOYZ3
 */

@Stateless
@LocalBean
public class ReservationSB {
    @Resource(name = "jdbc/flightreservation")
    DataSource dataSource;
    
    private ArrayList<ReservationSB> reservations = new ArrayList<>();
    
    public Boolean createReservation(Payment pay, Traveler t, Flight f) throws SQLException{
        pay = new Payment();
        t = new Traveler();
        f = new Flight();
        
        if (dataSource == null){
            throw new SQLException("Unable to obtain Datasource");
        }
      Connection connection = dataSource.getConnection();
      
        if (connection == null){
            throw new SQLException("Unable to connect to the DataSource");
        }
        
        try {
            String sql = "INSERT INTO `reservation`(`DepartureTime`, `ArrivalTime`, `FlyingHours`, `ArlineName`, `FlightNumber`, `AircraftName`, Status) VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement p = connection.prepareStatement(sql);
            p.setDate(1, f.getDepTimeFormated());
            p.setDate(2, f.getArvlTimeFormated());
            p.setTimestamp(3, f.getTotalFlightTimeFormated());
            p.setString(4, f.getAirlineName());
            p.setString(5, f.getFlightNumber());
            p.setString(6, f.getFlightNumber());
            p.setString(7, "Reserved");
            
            p.executeUpdate();
            
            return true;
        } catch(Exception ex){
            return false;
        }
    }
    
    public void DisplayConfirm(){
        
    }
    
    public Boolean blockTicket(Traveler t, Flight f) throws SQLException{
        t = new Traveler();
        f = new Flight();
        
        if (dataSource == null){
            throw new SQLException("Unable to obtain Datasource");
        }
      Connection connection = dataSource.getConnection();
      
        if (connection == null){
            throw new SQLException("Unable to connect to the DataSource");
        }
        
        try {
            String sql = "INSERT INTO `reservation`(`DepartureTime`, `ArrivalTime`, `FlyingHours`, `ArlineName`, `FlightNumber`, `AircraftName`, Status) VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement p = connection.prepareStatement(sql);
            p.setDate(1, f.getDepTimeFormated());
            p.setDate(2, f.getArvlTimeFormated());
            p.setTimestamp(3, f.getTotalFlightTimeFormated());
            p.setString(4, f.getAirlineName());
            p.setString(5, f.getFlightNumber());
            p.setString(6, f.getFlightNumber());
            p.setString(7, "Blocked");
            
            p.executeUpdate();
            
            startTimer(5.0);
            
            return true;
        } catch(Exception ex){
            return false;
        }
    }
    
    private void startTimer(Double time){
        
    }
}
