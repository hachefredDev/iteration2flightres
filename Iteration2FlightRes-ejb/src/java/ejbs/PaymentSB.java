/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import entities.Flight;
import entities.Payment;
import entities.Traveler;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author NOYZ3
 */

@Stateless
@LocalBean
public class PaymentSB {
    @Resource(name = "jdbc/flightreservation")
    DataSource dataSource;
    
    private ArrayList<PaymentSB> payments = new ArrayList<>();
    
    
    public Boolean createPayment(Payment pay) throws SQLException{
        pay = new Payment();
        
        if (dataSource == null){
            throw new SQLException("Unable to obtain Datasource");
        }
      Connection connection = dataSource.getConnection();
      
        if (connection == null){
            throw new SQLException("Unable to connect to the DataSource");
        }
        
        try {
            String sql = "INSERT INTO `payment`(`CCType`, `CCNumber`, `CCHolderName`, `CCExpiry`, `SecurityCode`, `AddrLine1`, `AddrLine2`) VALUES (?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement p = connection.prepareStatement(sql);
            p.setString(1, pay.getCcType());
            p.setString(2, pay.getCcNumber());
            p.setString(3, pay.getCcHolderName());
            p.setString(4, pay.getCcExpiry());
            p.setString(5, pay.getSecurityCode());
            p.setString(6, pay.getAddrLine1());
            p.setString(7, pay.getAddrLine2());
            
            p.executeUpdate();
            
            return true;
        } catch(Exception ex){
            return false;
        }
    }
}
