/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejbs;

import entities.Traveler;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.sql.DataSource;

/**
 *
 * @author NOYZ3
 */

@Stateless
@LocalBean
public class TravelerSB {
    @Resource(name = "jdbc/flightreservation")
    DataSource dataSource;
    
    private ArrayList<TravelerSB> travellers = new ArrayList<>();
    
    
    public Boolean createPayment(Traveler t) throws SQLException{
        t = new Traveler();
        
        if (dataSource == null){
            throw new SQLException("Unable to obtain Datasource");
        }
      Connection connection = dataSource.getConnection();
      
        if (connection == null){
            throw new SQLException("Unable to connect to the DataSource");
        }
        
        try {
            String sql = "INSERT INTO `traveler`(`FirstName`, `LastName`, `Gender`, `PhoneNumber`, `Email`, `PassportCountry`, `DateOfBirth`, `FrequentFlyerProgram`, `SpecialMealReq`, `OtherNeeds`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement p = connection.prepareStatement(sql);
            p.setString(1, t.getFirstName());
            p.setString(2, t.getLastName());
            p.setBoolean(3, t.getGender());
            p.setString(4, t.getPhoneNumber());
            p.setString(5, t.getEmail());
            p.setString(6, t.getPassportCountry());
            p.setDate(7, (Date) t.getDob());
            p.setString(8, t.getFrequentFlyerProgram());
            p.setString(9, t.getSpecialNeedReq());
            p.setString(10, t.getOtherNeeds());
            
            p.executeUpdate();
            
            return true;
        } catch(Exception ex){
            return false;
        }
    }
}
