/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;


import ejbs.FlightSB;
import ejbs.ReservationSB;
import entities.Airport;
import entities.Flight;
import entities.Payment;
import entities.Ticket;
import entities.Traveler;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.RequestScoped;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnectionFactory;
import javax.sql.DataSource;

/**
 *
 * @author frede
 */
@ManagedBean
@SessionScoped
public class UserData {

    @EJB
    private FlightSB flightSB;
    
    @EJB
    private ReservationSB resSB;
    
    @EJB
    private Traveler travelerSB;
    
    @EJB
    private Payment paymentSB;
    
    

    @Resource(mappedName = "StatusMessageTopicConnectionFactory")
    private TopicConnectionFactory statusMessageTopicCF;

    @Resource(mappedName = "StatusMessageTopic")
    private Topic statusTopic;

  @Resource(name = "jdbc/flightreservation")
    DataSource dataSource;
  
    private ArrayList<Flight> flights = new ArrayList<>();
    private ArrayList<Traveler> travelers = new ArrayList<>();
    //private Airport depairport = new Airport("YQM", "Moncton", "Moncton", "NB", "UTC-4");
    private Airport arrairport = new Airport("YYZ", "Toronto", "Toronto", "ON", "UTC-5");
    private Airport depairport;
    private ArrayList<Airport> airports = new ArrayList<>();
    private Flight flight = new Flight();

    private String flightDirection;
    private Date depDate;
    private Date retDate;
    private int adultNum;
    private int childNum;
    private int infantNum;
    private int ticketClass;
  
  
  
    private String flightNumber;
    private boolean isRendered = false;

    public ArrayList<Flight> getFlights() {
        return flightSB.getFlights();
    }

    public ArrayList<Airport> getAirports() throws SQLException {
        airports = flightSB.getAirportList();
        return airports;
    }

    public void setAirports(ArrayList<Airport> airports) {
        this.airports = airports;
    }

    
  public void addFlight() throws SQLException{
      boolean success = flightSB.addFlight(flights);
      if (success){
          System.out.println("Flight Added Successfully!");
      }
  }

  public void modifyFlight() throws SQLException{
      flightSB.modifyFlight(flight);
  }
  
  public void modifyFlightEx() throws SQLException{
     String message = flightSB.modifyFlightEx();
      System.out.println(message);
  }
    
  public void cancelFlight() throws SQLException{
      flightSB.cancelFlight();
      sendCancelledWarning();
  }
  
  public void deleteFlight() throws SQLException{
      flightSB.deleteFlight(flight);
  }
  
  public void getFlightById(String flightNumber) throws SQLException, Exception{
      flights = flightSB.getFlightById(flightNumber);
      isRendered = true;
  }

  public void searchFlights() throws Exception{
      System.out.println("Begining of Search Flight:!!!!!!!!!!!!!!!!!!!!!!!!!!");
      flights = flightSB.searchFlights(depairport, arrairport);
//flights = flightSB.searchFlights(this.getDepairport(), this.getArrairport());
      //isRendered = true;
      System.out.println("End of Search Flight");
  } 
  
  public void checkPrint(){
      System.out.println("Do you hit?");
  }
  
    public boolean isIsRendered() {
        return isRendered;
    }

    public void setIsRendered(boolean isRendered) {
        this.isRendered = isRendered;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public Airport getDepairport() {
        return depairport;
    }

    public void setDepairport(Airport depairport) {
        this.depairport = depairport;
    }

    public Airport getArrairport() {
        return arrairport;
    }

    public void setArrairport(Airport arrairport) {
        this.arrairport = arrairport;
    }

    public int getTicketClass() {
        return ticketClass;
    }

    public void setTicketClass(int ticketClass) {
        this.ticketClass = ticketClass;
    }



    public String getFlightDirection() {
        return flightDirection;
    }

    public void setFlightDirection(String flightDirection) {
        this.flightDirection = flightDirection;
    }

    public Date getDepDate() {
        return depDate;
    }

    public void setDepDate(Date depDate) {
        this.depDate = depDate;
    }

    public Date getRetDate() {
        return retDate;
    }

    public void setRetDate(Date retDate) {
        this.retDate = retDate;
    }

    public int getAdultNum() {
        return adultNum;
    }

    public void setAdultNum(int adultNum) {
        this.adultNum = adultNum;
    }

    public int getChildNum() {
        return childNum;
    }

    public void setChildNum(int childNum) {
        this.childNum = childNum;
    }

    public int getInfantNum() {
        return infantNum;
    }

    public void setInfantNum(int infantNum) {
        this.infantNum = infantNum;
    }
    
    public void sendCancelledWarning(){
        ArrayList<String> toAddress = new ArrayList<String>();
        
        for (Traveler t : travelers){
            toAddress.add(t.getEmail());
        }
        
       
        
        String from = "admin@flighthub.com";
        String content = 
                "Dear Customer,\n " +
                "We're sorry to announce that flight" + flight.getFlightNumber() +
                "has been cancelled.\n" +
                "Please contact a customer service representative to talk about your options.";

        try {
        
        javax.jms.Connection connection = statusMessageTopicCF.createConnection();
        
        connection.start();
        Session topicSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
        MessageProducer publisher = topicSession.createProducer(statusTopic);
        
        for (String to : toAddress){
            MapMessage message = topicSession.createMapMessage();
            message.setStringProperty("from", from);
            message.setStringProperty("to", to);
            message.setStringProperty("subject", "Your flight has been cancelled");
            message.setStringProperty("content", content);
            publisher.send(message);
        }
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
    
    
    
    public void sendReservationEmail(int ResId){
        ArrayList<String> toAddress = new ArrayList<String>();
        for (Traveler t : travelers){
            toAddress.add(t.getEmail());
        }
        
        ArrayList<String> passengers = new ArrayList<String>();
        for (Traveler t : travelers){
            passengers.add(t.getFirstName() + " " + t.getLastName() + " | G: " + t.getGender() + " | Phone number: " + t.getPhoneNumber() + " | Country of passport: " + t.getPassportCountry() + " | DOB: " + t.getDob());
        }
        
        StringBuilder sbPass = new StringBuilder();
        for(String s : passengers){
            sbPass.append(s);
            sbPass.append("\n");
        }
        
        //Add payment details
        String from = "admin@flighthub.com";
        String content = 
                "Dear Customer,\n " +
                "Thank you for choosing us as your flight provider. Listed below is all your flight information for your upcoming trip.\nReservation ID: " + ResId + "\nDeparture airport: " + flight.getDepIATA() + "\nArrival airport: " + flight.getArvlIATA() + "\nDeparture Time: " + flight.getDepDate() + "\nArrival time: " + flight.getArvlDate() + "\nTotal flight time: " + flight.getTotalFlightTime() + "\nAirline name: " + flight.getAirlineName() + "\nFlight number: " + flight.getFlightNumber() + "\n\nPassengers: " + sbPass.toString();

        try {
        
        javax.jms.Connection connection = statusMessageTopicCF.createConnection();
        
        connection.start();
        Session topicSession = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        
        MessageProducer publisher = topicSession.createProducer(statusTopic);
        
        for (String to : toAddress){
            MapMessage message = topicSession.createMapMessage();
            message.setStringProperty("from", from);
            message.setStringProperty("to", to);
            message.setStringProperty("subject", "Confirmation email");
            message.setStringProperty("content", content);
            publisher.send(message);
        }
        }
        catch (JMSException e) {
            e.printStackTrace();
        }
    }
    
    public Boolean makePayment(Payment p){
        //return paymentSB.createPayment(p);
        return true;
    }
    
    public Boolean blockTicket(Traveler t, Flight f){
        try {
            return resSB.blockTicket(t, f);
        } catch(Exception ex){
            return false;
        }
    }
    
    public Boolean createRes(Traveler t, Flight f, Payment p){
        try {
            return resSB.createReservation(p, t, f);
        } catch(Exception ex){
            return false;
        }
    }
}
